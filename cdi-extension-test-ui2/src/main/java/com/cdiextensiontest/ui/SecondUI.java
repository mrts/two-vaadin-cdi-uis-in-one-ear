package com.cdiextensiontest.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDINavigator;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.UIScoped;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.inject.Inject;

@CDIUI("cdi-extension-test2")
@Theme("cdi-extension-test")
@PushStateNavigation
@UIScoped
public class SecondUI extends UI {

//    @Inject
//    private CDINavigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        setContent(layout);

        Button button = new Button("Click");
        button.addClickListener(event -> layout.addComponent(new Label("Clicked")));
        layout.addComponent(button);

//        layout.setMargin(false);
//        navigator.init(this, layout);
    }

}
