two-cdi-uis-in-one-ear
======================

Vaadin 8 + Java EE 8 application with two CDI UI WARs in one EAR. Demonstrates
the problem and workaround described in https://github.com/vaadin/cdi/issues/97
and https://github.com/vaadin/cdi/issues/282.

Quoting the problem:

> *CDI extensions are always loaded in the scope of the EAR. They will only be
> applied once, which means that you can only register a scope (`@UIScope` in
> this case) once for the entire EAR. This means that whenever you try to find
> objects from `@UIScope`, you'll call into the scope of one single WAR, even if
> you initiate access from a different WAR. This means that CDI for Vaadin apps
> will only work reliably in one single WAR inside an EAR.*

Quoting the workaround:

> The workaround seems to be to simply add the `@UIScope` annotation directly
> to the UI classes (next to the `@CDIUI` annotation). By doing this, Weld
> follows a different path to look up the scope and seems to find the right
> scope from the right class loader.

Without the workaround, deploying the EAR of this project to WildFly 15 results
in the following exception:

    [2018-12-20 04:39:55,487] Artifact cdi-extension-test-ear:ear: Error during artifact deployment. See server log for details.
    [2018-12-20 04:39:55,489] Artifact cdi-extension-test-ear:ear: java.lang.Exception: {"WFLYCTL0080: Failed services" => {"jboss.deployment.unit.\"cdi-extension-test-ear.ear\".WeldStartService" => "Failed to start service
        Caused by: org.jboss.weld.exceptions.DefinitionException: Exception List with 1 exceptions:
    Exception 0 :
    [CDIUI_SCOPE] Inconsistent deployment: The CDI UI class com.cdiextensiontest.ui.FirstUI should be @UIScoped.
        at com.vaadin.cdi.internal.VaadinExtension.throwInconsistentDeployment(VaadinExtension.java:93)
        at com.vaadin.cdi.internal.VaadinExtension.processManagedBean(VaadinExtension.java:86)
        at sun.reflect.GeneratedMethodAccessor16.invoke(Unknown Source)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.jboss.weld.injection.StaticMethodInjectionPoint.invoke(StaticMethodInjectionPoint.java:95)
        at org.jboss.weld.injection.MethodInvocationStrategy$SpecialParamPlusBeanManagerStrategy.invoke(MethodInvocationStrategy.java:144)
        at org.jboss.weld.event.ObserverMethodImpl.sendEvent(ObserverMethodImpl.java:330)
        at org.jboss.weld.event.ExtensionObserverMethodImpl.sendEvent(ExtensionObserverMethodImpl.java:123)
        at org.jboss.weld.event.ObserverMethodImpl.sendEvent(ObserverMethodImpl.java:308)
        at org.jboss.weld.event.ObserverMethodImpl.notify(ObserverMethodImpl.java:286)
        at javax.enterprise.inject.spi.ObserverMethod.notify(ObserverMethod.java:124)
        at org.jboss.weld.util.Observers.notify(Observers.java:166)
        at org.jboss.weld.event.ObserverNotifier.notifySyncObservers(ObserverNotifier.java:285)
        at org.jboss.weld.event.ObserverNotifier.notify(ObserverNotifier.java:273)
        at org.jboss.weld.event.ObserverNotifier.fireEvent(ObserverNotifier.java:177)
        at org.jboss.weld.event.ObserverNotifier.fireEvent(ObserverNotifier.java:171)
        at org.jboss.weld.bootstrap.events.AbstractContainerEvent.fire(AbstractContainerEvent.java:53)
        at org.jboss.weld.bootstrap.events.AbstractDefinitionContainerEvent.fire(AbstractDefinitionContainerEvent.java:44)
        at org.jboss.weld.bootstrap.events.ProcessManagedBeanImpl.fire(ProcessManagedBeanImpl.java:31)
        at org.jboss.weld.bootstrap.events.ContainerLifecycleEvents.fireProcessBean(ContainerLifecycleEvents.java:242)
        at org.jboss.weld.bootstrap.events.ContainerLifecycleEvents.fireProcessBean(ContainerLifecycleEvents.java:236)
        at org.jboss.weld.bootstrap.AbstractBeanDeployer.fireProcessBeanEvents(AbstractBeanDeployer.java:128)
        at org.jboss.weld.bootstrap.BeanDeployer.deploy(BeanDeployer.java:331)
        at org.jboss.weld.bootstrap.BeanDeployment.deployBeans(BeanDeployment.java:264)
        at org.jboss.weld.bootstrap.WeldStartup.deployBeans(WeldStartup.java:447)
        at org.jboss.weld.bootstrap.WeldBootstrap.deployBeans(WeldBootstrap.java:86)

Project Structure
=================

The project consists of the following three modules:

- parent project: common metadata and configuration
- `cdi-extension-test-ui1`: Vaadin web application (WAR) that contains the first UI
- `cdi-extension-test-ui2`: Vaadin web application (WAR) that contains the second UI
- `cdi-extension-test-ear`: EAR project that bundles the whole application together

Running
=======

To compile the entire project, run `mvn package` in the parent project. Deploy
the resulting EAR to a WildFly 15 application server.
